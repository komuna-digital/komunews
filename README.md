# Komunews

This is a mastodon bot for toot news from selected rss feeds, used in [komuna.digital](https://komuna.digital).

## Build and Run

```bash
pip install .
komunews
```

## Environment Variables

You must set these environment variables:

`KOMUNEWS_BASE_URL`: URL from your instance.

`KOMUNEWS_ACCESS_TOKEN`: Your mastodon account access token.

`KOMUNEWS_URLS_PATH`: Path to a file with your desired URL feeds.


Optional:

`KOMUNEWS_CHECK_PERIOD`: Set time (in seconds) between each check. (default: 1800)
