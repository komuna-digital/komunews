{
  description = "Komuna.digital news bot";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  outputs = { nixpkgs, ... }:
    let
      inherit (nixpkgs) lib;
      pkgsFor = nixpkgs.legacyPackages;
      name = "komunews";
      genSystems = lib.genAttrs [
        "aarch64-linux"
        "x86_64-linux"
      ];
    in
    rec {
      overlays.default = final: prev: rec {
        ${name} = final.callPackage ./default.nix { };
      };

      packages = genSystems (system:
        let
          pkgs = import nixpkgs {
            inherit system;
            overlays = [ overlays.default ];
          };
        in
        rec {
          komunews = pkgs.${name};
          default = komunews;
        });


      nixosModules.default = import ./module.nix;
    };
}
