from datetime import datetime, timedelta
from komunews import app, last, urls
from mastodon import MastodonError
import time
import os
import feedparser

def getNewArticles(since: datetime=datetime.now()):
    stack = []

    for url in urls:
        NewsFeed = feedparser.parse(url)
        lastTime = datetime.utcfromtimestamp(last.getLastTime(url))
        print(f'checking for {url}')
        for news in NewsFeed.entries:
            newsDate = datetime(*(news.published_parsed[:-2]))
            if newsDate <= lastTime:
                break
            stack.append((news, url, newsDate))

    stack.sort(key=lambda x: x[1], reverse=True)
    while len(stack):
        news, url, date = stack.pop()
        try:
            text = f'{news.title}\n{news.link}'
            app.toot(text)
            last.saveLastTime(url, date)
            print(f'Published: {text}')
        except MastodonError as e:
            print(e)
        time.sleep(0.1)
