from komunews import urls, HOME
from datetime import datetime, timedelta
import json
import sys

LASTS_PATH = f'{HOME}/lasts.json'

def fileExists(fname: str) -> bool:
    try:
        with open(fname) as f: pass
        return True
    except FileNotFoundError:
        return False

def createLastsFile():
    try:
        with open(LASTS_PATH, 'w') as f:
            lasts = {}
            for url in urls:
                lasts[url] = datetime.utcnow().timestamp()
            json.dump(lasts, f, indent=4)

    except FileNotFoundError:
        print('Path does not exists')
        sys.exit(1)

def saveLastTime(url: str, timestamp: datetime):
    if not fileExists(LASTS_PATH):
        createLastsFile()
    with open(LASTS_PATH, 'r') as f:
        lasts = json.load(f)
        try:
            if lasts[url] > timestamp.timestamp():
                return
        except KeyError: pass
    with open(LASTS_PATH, 'w') as f:
        lasts[url] = timestamp.timestamp()
        json.dump(lasts, f, indent=4)

def getLastTime(url: str) -> datetime:
    if not fileExists(LASTS_PATH):
        createLastsFile()
    timestamp = datetime.utcnow().timestamp()
    with open(LASTS_PATH, 'r') as f:
        lasts = json.load(f)
        try:
            timestamp = lasts[url]
        except KeyError:
            f.close()
            saveLastTime(url, timestamp)
    return timestamp
