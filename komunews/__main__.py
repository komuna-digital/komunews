from datetime import datetime, timedelta
from komunews.feeds import getNewArticles
from datetime import datetime
import time
import os

PERIOD = int(os.environ.get('KOMUNEWS_CHECK_PERIOD', 1800))

def main():
    time_d = timedelta(seconds=PERIOD)

    print('Running Komunews...')
    while True:
        print('Checking for new articles...')
        getNewArticles(since=datetime.now())
        print('All news sent!')
        time.sleep(PERIOD)

if __name__ == '__main__':
    main()
