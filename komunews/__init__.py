from mastodon import Mastodon
import feedparser
import os
import toml

BASE_URL = os.environ.get('KOMUNEWS_BASE_URL')
CLIENT_ID = os.environ.get('KOMUNEWS_CLIENT_ID')
ACCESS_TOKEN = os.environ.get('KOMUNEWS_ACCESS_TOKEN')
URLS_PATH = os.environ.get('KOMUNEWS_URLS_PATH', '/var/komunews/urls')
HOME = os.environ.get('HOME')

urls = []
with open(URLS_PATH) as f:
    urls = toml.load(f)['feeds']

if not ACCESS_TOKEN:
    with open(os.environ['KOMUNEWS_ACCESS_TOKEN_FILE']) as f:
        ACCESS_TOKEN = f.read().strip()

app = Mastodon(
    access_token = ACCESS_TOKEN,
    api_base_url = BASE_URL
)
