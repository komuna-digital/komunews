{ lib, python310Packages, pkgs, callPackage, ... }:

let
  vnumber = "0.2.0";
in
python310Packages.buildPythonPackage rec {

  pname = "komunews";
  version = vnumber;

  format = "pyproject";

  src = ./.;

  buildInputs = with pkgs.python310Packages; [
    setuptools
  ];

  propagatedBuildInputs = with pkgs.python310Packages; [
    mastodon-py
    feedparser
    toml
  ];

  meta = with lib; {
    description = "Komuna.digital mastodon bot for tootting news";
    homepage = "https://gitlab.com/komuna-digital/komunews";
    license = licenses.asl20;
    platforms = platforms.all;
  };

}
