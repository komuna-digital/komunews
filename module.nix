{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.services.komunews;
  settingsFormat = pkgs.formats.toml {};
in {

  options.services.komunews = {
    enable = mkEnableOption "komunews services";

    package = mkOption {
      type = types.package;
      default = pkgs.komunews;
    };

    user = mkOption {
      type = types.str;
      default = "komunews";
    };

    home = mkOption {
      type = types.str;
      default = "/var/komunews/";
    };

    feedsURLs = mkOption {
      type = types.listOf types.str;
      default = [ ];
    };

    baseAPIUrl = mkOption {
      type = types.str;
      default = null;
    };

    checkPeriod = mkOption {
      type = types.int;
      default = 1800;
    };

    accessTokenFile = mkOption {
      type = types.str;
      default = null;
    };

  };

  config = mkIf cfg.enable {

    environment.etc."komunews.toml".source =
      settingsFormat.generate "config.tml" { feeds = cfg.feedsURLs; };
    users.users.${cfg.user} = {
      inherit (cfg) home;
      isSystemUser = true;
      group = cfg.user;
      createHome = true;
    };

    users.groups.${cfg.user} = {};

    systemd.services.komunews = {
      description = "Komunews";
      wantedBy = [ "multi-user.target" ];
      environment = {
        KOMUNEWS_BASE_URL = cfg.baseAPIUrl;
        KOMUNEWS_ACCESS_TOKEN_FILE = cfg.accessTokenFile;
        KOMUNEWS_URLS_PATH =  "/etc/komunews.toml";
        KOMUNEWS_CHECK_PERIOD = "${toString cfg.checkPeriod}";
      };
      serviceConfig = {
        ExecStart = "${cfg.package}/bin/komunews";
        Restart = "on-failure";
        User = cfg.user;
      };
    };
  };
}
